var categories = [];
var markers = new L.MarkerClusterGroup({spiderfyDistanceMultiplier: 2.2, maxClusterRadius: 40, disableClusteringAtZoom: 12});
var markers2 = new L.MarkerClusterGroup({maxClusterRadius: 40, showCoverageOnHover: false});
var colors = {
	"antisemitismo": "#874321", 
	"aporofobia": "#f0e52b", 
	"homofobia": "#5e457b", 
	"otros": "#f8ac00", 
	"islamofobia": "#4faf42", 
	"odioideologico": "#c26b35", 
	"racismoxenofobia": "#db1c46", 
	"transfobia": "#bb61a2", 
	"futbol": "#29b0e6", 
	"disfobia": "#61873F", 
	"romafobia": "#98B321"
};
var isMainland = true;

var x = 35, //x = 40,
	y = -1,
	zoom = 5,
	minZoom = 4;
window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};
if (window.mobilecheck()) {
	x = 37;
	y = -6;
	zoom = 5;
}

/* main map */
var map = L.map('map').setView([x,y], zoom);
map.once('focus', function() { map.scrollWheelZoom.enable(); });

L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}.png', {
	minZoom: 4,
	maxZoom: 18,
	//id: id,
	//token: token
	attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, © <a href="https://carto.com/about-carto/">CARTO</a>',
}).addTo(this.map);

L.control.attribution({
  position: 'bottomleft'
}).addTo(map);

/* canary island map */
/*var xCanary = 28.1,
	yCanary = -15.4,
	zoomCanary = 5;
var map2 = L.map('map2', { zoomControl:false, attributionControl:false, dragging:false }).setView([xCanary, yCanary], zoomCanary);

L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}.png', {
	maxZoom: zoomCanary,
	minZoom: minZoom,
}).addTo(this.map2);

map2.on('click', function(e) {
	if (isMainland) {
		map.setView([xCanary+5, yCanary+10], zoomCanary);
		map2.setView([x,y-3], minZoom);
	}
	else {
		map.setView([x,y], zoom);
		map2.setView([xCanary, yCanary], zoomCanary);
	}
	isMainland = !isMainland;
});*/

/* add overlay button for mobile */
if (window.mobilecheck()) {
	L.easyButton( '<span class="star">&equiv;</span>', function(){
	  jQuery("#contentfilter").toggle();
	}).addTo(map);
}

// Fetch, process and display geoJSON.
jQuery.when(
	jQuery.getJSON("/"+getLang()+"/geojson", {})   
	.done (function( hatecrimes ) {

		loadRegisters(hatecrimes.features);
		//loadSlider(hatecrimes.features)
		initFilter();

	})
    .fail(function(hatecrimes) {    
      console.log("json error");
    })
).then(function() { 
	console.log("json loaded!");
});

function loadRegisters(json) {
	for (key in json) {
		var register = json[key].properties;
		register.longitude = json[key].geometry.coordinates[0];
		register.latitude = json[key].geometry.coordinates[1];
		//console.log(register);

		// load categoria object
		var catTitle = register.category.trim().split(",");
		var catSlug = register.catSlug.trim().split(",");
		var cats = new Array();

		if( Object.prototype.toString.call( catSlug ) === '[object Array]' ) {
		    catSlug.forEach(function (category, index) {
		    	cats.push(registerCategory(catTitle[index].trim(), category.trim()));
		    });
		} else {
			cats.push(registerCategory(catTitle.trim(), catSlug.trim()));
		}

		//create marker for register
		if (isNumber(register.latitude) && isNumber(register.longitude)) {
			var dateStr = register.date.split("/");
			var date = new Date(Date.UTC(dateStr[2], dateStr[1]-1, dateStr[0]));
			const options = { year: 'numeric', month: 'long', day: 'numeric' };
			if (getLang() == "ca")
				date = date.toLocaleDateString('ca-ES', options);
			else if (getLang() == "es")
				date = date.toLocaleDateString('es-ES', options);
			else if (getLang() == "en")
				date = date.toLocaleDateString('en-US', options);

			var popupText = "<h2>"+register.title+"</h2>";
			popupText += "<p>"+date+".<br>"+register.city+"</p>";

			/*var desc = register.description;
			desc = desc.substr(0, 400);
			desc = register.description.substr(0, Math.min(desc.length, desc.lastIndexOf(". ")+1));
			popupText += "<p>"+desc+"</p>";*/
			popupText += "<a class='info' target='_parent' href='/"+getLang()+"/?p="+register.id+"'>+INFO</a>";
			popupText += "<p><strong>"+register.category+"</strong></p>";

			var marker = L.marker(new L.LatLng( register.latitude, register.longitude ), {
				icon: cats[0].icon,
				title: register.title
			});

			// canary islands without popup
			markers2.addLayer(marker);

			// main land
			marker.bindPopup(popupText);
			//console.log("Add register ", register);

			// save marker to layers
			cats.forEach(function (category, index) {
				category.layer.addLayer(marker);
			});
			markers.addLayer(marker);
		}
		else {
			console.log("WARNING! latitude or longitude contains error, can't show this place: ["+register.id+"] "+register.title+" | cat:"+register.category);
		}			

	}
	map.addLayer(markers);
	//map2.addLayer(markers2);
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

// get category object by category name
function getCat(cat) {
    for (var i = 0, len = categories.length; i < len; i++) {
        if (categories[i].slug === cat)
            return categories[i];
    }
    return null;
}

function registerCategory(catTitle, catSlug) {
	cat = getCat(catSlug);

	if (cat == null) {
		//create new category
		//console.log("Add category: "+catTitle+" ["+catSlug+"]");
		cat = {
			title: catTitle,
			slug: catSlug,
			color: colors[catSlug],
			layer: new L.LayerGroup(),
			icon: new L.icon({
				iconUrl: '/wp-content/plugins/hatecrimes-map/images/'+catSlug+'.png',
				iconSize:     [24, 32],
				iconAnchor:   [12, 16],
				popupAnchor:  [0, -16],
				color: colors[catSlug]
			})
		};
		categories.push(cat);
	}

	return cat;
}

/**************
 filter 
 **************/
function initFilter() {
	//categories.sortOn("title");
	categories.sort(sortOn("title"));

	categories.forEach(function(cat, i){
		jQuery('#filter').append('<p><input class="cb cb-cat" id="cb-cat-'+cat.id+'" type="checkbox" value="'+cat.slug+'" checked="checked"><span style="color:#fff;background-color:'+cat.color+'">'+cat.title+'</span></p>');
	});

	//add feminicidio & antisemitismo
	//jQuery('#filter').append('<a target="_blank" href="http://www.informeraxen.es/tag/antisemitismo/" class="cb cb-cat" style="color:#fff;background-color:#874321;margin-left:21px;">'+getLangString("Antisemitism")+'</a></br>');
	jQuery('#filter').append('<a target="_blank" href="http://www.feminicidio.net/menu-feminicidio-informes-y-cifras" class="cb cb-cat" style="color:#fff;background-color:#a00;margin-left:21px;">'+getLangString("Misogyny")+'</a></br></br>');

	// category changed
	jQuery('.cb-cat').on('change', function(e) {
		var id = jQuery(this).attr('id');
		var cb = '';
		if (jQuery(this).is(':checked')) cb = 'checked';
		//select subcats
		jQuery('.'+id).prop('checked', cb);
		
		showCats();
	});

	jQuery('#filter').append('<p class="buttons"><button type="button" onclick="showAll();">'+getLangString('All')+'</button> <button type="button" onclick="showNone();">'+getLangString('None')+'</button></p>');
}

function showCats() {
	var cats=new Array();
	jQuery('#filter input:checked').each(function(){
		cats.push(jQuery(this).val());
	});
	showCat(cats);
}

// show all layers
function showAll() {
	jQuery('#filter').find(':checkbox').prop('checked', 'checked');
	markers.clearLayers();
	//markers2.clearLayers();
	categories.forEach(function(obj,index){
		markers.addLayer(categories[index].layer);
		//markers2.addLayer(categories[index].layer);
	});
}

// hide all layers
function showNone() {
	jQuery('#filter').find(':checkbox').prop('checked', '');
	markers.clearLayers();
}

// show only this category
function showCat(cats) {
	markers.clearLayers();
	cats.forEach(function(cat,i){
		categories.forEach(function(obj,j){
			//console.log(obj.slug, cat);
			if (obj.slug === cat) {
				markers.addLayer(obj.layer);
			}
		});
	});
}

/**************
 help functions 
 **************/
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+jQuery/g;
String.prototype.trim = function() {
	return this.replace(rtrim, '');
}

function sortOn(key) {
  return function(a,b){
   if (a[key] > b[key]) return 1;
   if (a[key] < b[key]) return -1;
   return 0;
  }
}

//get language from URL
function getLang() {
	var language = "ca";
	var loc = window.location.href;
	var url = "crimenesdeodio.info/"
	var pos1 = loc.indexOf(url);
	loc = loc.substring(pos1+url.length);
	loc = loc.split("/");
	if (loc[0] == "en" || loc[0] == "es") {
		language = loc[0];
	}
	return language;
}

function getLangString(str) {
	if (getLang() == "en") {
		return str;
	} else {
		return translations[str][getLang()];
	}
}

var translations = {
	"All": {
		"es": "Todos",
		"ca": "Tots" 
	},
	"None": {
		"es": "Ninguno",
		"ca": "Ningú" 
	},
	"type": {
		"es": "Tipología",
		"ca": "Tipologia"
	},
	"Antisemitism": {
		"es": "Antisemitismo",
		"ca": "Antisemitisme"
	},
	"Misogyny": {
		"es": "Misoginia",
		"ca": "Misogínia"
	}
}
